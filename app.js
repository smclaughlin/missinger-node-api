
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.bodyParser());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.post('/register',routes.postRegister);

app.post('/createreport', routes.auth, routes.postReport);
app.put('/report/:id', routes.auth, routes.putReport);
app.get('/user/reports', routes.auth, routes.getUserReports);
app.get('/reports', routes.auth, routes.getAllReports);
app.del('/report/:id', routes.auth, routes.delReport);
app.get('/user/email/:id', routes.auth, routes.getUserEmailById);
app.post('/login', routes.login);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
