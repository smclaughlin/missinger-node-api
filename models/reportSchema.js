/*Report Schema*/
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var ReportSchema = new Schema({
	name : String,
	user_id : String,
	report_details : String,
	missing_date : String,
	created_at : String,
	category : String,
	status : String, 
	reward : String, 
	country : String,
	image_blob : String
});

exports.ReportSchema = ReportSchema;

