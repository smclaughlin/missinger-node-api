/*User schema*/

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;


var UserSchema = new Schema({
	firstname : String, 
	lastname : String, 
	email : {type: String, index: {unique: true}},
	password : String, 
	created_at : String, 
	api_key : String,
	salt : String,
	hash : String
});

exports.UserSchema = UserSchema;