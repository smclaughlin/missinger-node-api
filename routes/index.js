
/*
 * GET home page.
 */

var mongoose = require('mongoose');
var yaml_config = require('node-yaml-config');
var uuid = require('node-uuid');
var bcrypt = require('bcrypt');

var config = yaml_config.load(process.cwd()  + '/config/config.yml');

var ReportSchema = require('../models/reportSchema').ReportSchema;
var UserSchema =  require('../models/userSchema').UserSchema;

var salt = bcrypt.genSaltSync(10);
var hash = bcrypt.hashSync("my password", salt);

var WORK_FACTOR = 10;

var conn = mongoose.createConnection(config.db);

var ReportModel = conn.model('Report', ReportSchema);
var UserModel = conn.model('User', UserSchema);

conn.on("error", function(errorObject){
  console.log(errorObject);
});

conn.once('open', function callback () {
	console.log("loading item route");
})

//Authorization route middleware
exports.auth = function(req, res, next){
	var apiKey = req.get('Authorization');
	console.log("Attempting access with apiKey", apiKey);
	if(apiKey){
		UserModel.findOne({api_key : apiKey}, function(err, user){
			if(err || !user){
				res.send({error: true, message : "Unauthorized"}, 401);
			} else {
				console.log("found user", user);
				var userObj = {
					userId : user._id
				};

				req.user = userObj;
				next();
			}
		});
	} else {
		res.send({error: true, message : "Api key is misssing"}, 400);
	}
	
};


exports.postReport = function(req, res){
	var report = new ReportModel();
	
	report.name = req.body.name;
	report.user_id = req.user.userId;
	report.report_details = req.body.report_details;
	report.missing_date = req.body.missing_date;
	report.created_at = Date.now();
	report.category = req.body.category;
	report.status = req.body.status; 
	report.reward = req.body.reward;
	report.country = req.body.country;
	report.image_blob = req.body.image_blob;

	report.save(function(err, report){
		if(err){
			return res.send(500, {message : "Error occured while creating report", error : true});
		} else {
			
			return res.send(201, {message : "Created", error : false});
		}
	});
};

exports.putReport = function(req, res){
	var reportId = req.params.id;

	ReportModel.findOne({_id : reportId, user_id : req.user.userId}, function(err, report){
		if(err){
			return res.send("some error message");
		} else {
			report.name = req.body.name;
			report.report_details = req.body.report_details;
			report.missing_date = req.body.missing_date;
			report.category = req.body.category;
			report.status = req.body.status; 
			report.reward = req.body.reward;
			report.country = req.body.country;
			report.image_blob = req.body.image_blob;

			report.save(function(err, report){
				if(err){
					return res.send(500, {error : true, message : "Report failed to update. Please try again!"});
				} else {
					return res.send(200, {error : false, message : "Report has been successfully updated"});
				}
			});
		}

	})
};

exports.delReport = function(req, res){
	ReportModel.findOne({_id: req.params.id, user_id : req.user.userId}).remove(function(err){
		if(err){
			return res.send(500,{error : true, message : "Failed to delete report"});
		} else {
			return res.send(200,{error : false, message : "Report successfully deleted"});
		}
	});
};

exports.getUserReports = function(req, res){
	ReportModel.find({user_id : req.user.userId}, function(err, reports){
		if(err){
			res.send("some error message");
		} else {
			res.send(200, {reports : reports, error : false});
		}
	});
};

exports.getAllReports = function(req, res){
	ReportModel.find({}, function(err, reports){
		if(err){
			res.send(500,{error : true, message : "Error occured while retrieving report"});
		} else {
			res.send(200, {reports : reports, error : false});
		}
	});
};

exports.postRegister = function(req, res){
	var user = new UserModel();
	var salt = bcrypt.genSaltSync(WORK_FACTOR);

	user.lastname = req.body.lastname;
	user.firstname = req.body.firstname;
	user.salt = salt;
	user.hash = bcrypt.hashSync(req.body.password, salt);
	user.email = req.body.email;
	user.created_at = Date.now();
	console.log(req.body);
	user.api_key = uuid.v4();

	user.save(function(err, user){
		if(err){
			res.send({error : true, message: "An error has occured"});
		} else {
			var filteredUser = filterUserModel(user);
			res.send(200, {user : filteredUser, message : "Account has successfully been created", error : false});
		}
	});
}

exports.login = function(req,res){
	if(	typeof req.body.email == "undefined" || 
		!req.body.email|| 
		typeof req.body.password == "undefined" ||
		!req.body.password){
		
		return res.send(401);
	}

	UserModel.findOne({email : req.body.email}, function(err, user){
		if(err || !user){
			res.send(401,{error : true, message : "Incorrect credentials"});
		} else {
			
			if(bcrypt.compareSync(req.body.password, user.hash)){
				var filteredUser = filterUserModel(user);
				
				res.send(200, {error : false, user : filteredUser});
			} else {
				res.send(401,{error : true, message : "Incorrect credentials"});
			}
		}
	});
}

exports.getUserEmailById = function(req, res){
	if(req.params.id){
		var id = req.params.id;
		UserModel.findById(id, function(err, user){
			if(err || !user){
				res.send(500, {error : true, message : "An error has occured"});
			} else {
				res.send(200, {error : false, email : user.email});
			}
		});
	}
}

//Helper function to filter user objects before sending to browser
function filterUserModel(user){
	user.hash = undefined;
	user.salt = undefined;

	return user;
}



